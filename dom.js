let todos = [];
// let flag = false;
let newArray = []
getTodos()
refresh()
let filterBtn = document.getElementsByClassName('filterItem')
filterBtn[0].addEventListener('click', filterList)


function filterList(e) {
    // flag = true
    newArray = todos.filter(item => item.status === false)
    todos = newArray;
    refresh()
}


// Form submit event 
var form = document.getElementById('addForm')
form.addEventListener('submit', addItem);


// Add items
function addItem(e) {
    e.preventDefault();
    // Get Input value
    var newItem = document.getElementById('item').value;
    let arr = todos.filter(items => items.text === newItem.toLowerCase())
    if (newItem != '' && arr.length == 0) {
        let obj = {
            text: newItem,
            status: false
        }
        todos.push(obj)
        saveTodos()
        refresh()
    } else if (arr.length != 0) {
        alert('This list already Exist')
        document.getElementById('item').value = null;

    }
    // console.log(todos)

}


function removeItem(e) {
    if (e.target.classList.contains('delete')) {
        var li = e.target.parentElement
        let index = li.getAttribute('position');
        todos.splice(index, 1);
        saveTodos()
        refresh()
    }
}


function checkItem(e) {
    var parent = e.target.parentNode;
    let index = parent.getAttribute('position');
    // console.log(index)
    todos[index].status = e.target.checked;
    saveTodos();
    refresh();
}


// drag functions
let startPoint;
let endPoint;

function dragStart(e) {
    startPoint = e.srcElement.getAttribute('position');
    // console.log(dragStartPosition)
}

function dragOver(e) {
    endPoint = e.target.getAttribute('position');
    //  console.log(endPoint)
}

function dragEnd() {
    const content = todos.splice(startPoint, 1);
    // console.log(content)
    todos.splice(endPoint, 0, content[0]);
    saveTodos();
    refresh();
}


// Store data to local storage:
function saveTodos() {
    let str = JSON.stringify(todos);
    localStorage.setItem('todos', str)
}


function getTodos() {
    let str = localStorage.getItem('todos')
    todos = JSON.parse(str)
}




function refresh() {
    const items = document.getElementById('items')

    while (items.hasChildNodes()) {
        items.lastChild.remove();
    }


    // console.log(todos)

    for (let i = 0; i < todos.length; i++) {
        // if(todos[i].status == false){

        var li = document.createElement('li')

        // Add class
        li.className = 'list-group-item'

        // create a checkbox element:
        var check_box = document.createElement('input')

        // Add class to delete btn:
        check_box.className = "check";
        check_box.checked = todos[i].status;
        check_box.type = 'checkbox';
        check_box.addEventListener('change', checkItem);

        // Append to li:
        li.appendChild(check_box);

        // create a btn element:
        var del_btn = document.createElement('button')

        // Add class to delete btn:
        del_btn.className = "delete"

        // Append Text node:
        del_btn.appendChild(document.createTextNode('X'))

        // delete an item
        del_btn.addEventListener('click', removeItem);

        // Add textNode with Input value:
        li.appendChild(document.createTextNode(todos[i].text))

        // append btn to li:
        li.appendChild(del_btn)

        // dragable
        li.draggable = 'true';

        // Append li to list:
        items.appendChild(li);
        // console.log();

        // todos.push(li)
        li.setAttribute('position', todos.indexOf(todos[i]))
        // console.log(todos)
        document.getElementById('item').value = null;
        // checkitems

        if (todos[i].status === true) {
            li.style.textDecoration = "line-through";
            items.addEventListener('dragstart', dragStart);
            items.addEventListener('dragover', dragOver);
            items.addEventListener('dragend', dragEnd);

            // }
        }

    }
}